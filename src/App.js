import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Dashboard from "./component/Nav";
import Form from "./component/Form";
import Table from "./component/Table";
import Register from "./component/Register";
import Login from "./component/Login";
import { useState } from "react";

function App() {
  const [data, setData] = useState([]);
  return (
    <div className="App">
      <Router>
        <Routes>
          <Route exact path="/" element={<Register />} />
          <Route path="/login" element={<Login />} />
          <Route path="/dashboard" element={<Dashboard />} />
          <Route
            path="/form"
            element={<Form data={data} setData={setData} />}
          />
          <Route
            path="/table"
            element={<Table data={data} setData={setData} />}
          />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
