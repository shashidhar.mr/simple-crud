import React, { useEffect, useState } from "react";
import Nav from "./Nav";

function Form({ data, setData }) {
  const [userData, setUserData] = useState({
    name: "",
    qualBe: false,
    qualMtech: false,
    gender: "",
    country: "",
  });

  const onSubmit = (e) => {
    e.preventDefault();
    debugger;
    console.log(userData);
    const userDetails = JSON.parse(JSON.stringify(data));
    setData([...userDetails, userData]);
  };

  useEffect(() => {
    debugger;
    console.log(data, "data");
  }, [data]);

  return (
    <div>
      <Nav />
      <form onSubmit={onSubmit}>
        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="inputEmail4">Name</label>
            <input
              type="text"
              class="form-control"
              id="inputEmail4"
              value={userData.name}
              onChange={(e) =>
                setUserData({ ...userData, name: e.target.value })
              }
            />
          </div>
          <div class="form-group col-md-6">
            <label for="inputPassword4">Qualification</label>
            <br />
            <div class="form-check form-check-inline">
              <input
                class="form-check-input"
                type="checkbox"
                id="inlineCheckbox1"
                checked={userData.qualBe}
                onChange={() =>
                  setUserData({ ...userData, qualBe: !userData.qualBe })
                }
              />
              <label class="form-check-label" for="inlineCheckbox1">
                B .E
              </label>
            </div>
            <div class="form-check form-check-inline">
              <input
                class="form-check-input"
                type="checkbox"
                id="inlineCheckbox2"
                checked={userData.qualMtech}
                onChange={() =>
                  setUserData({ ...userData, qualMtech: !userData.qualMtech })
                }
              />
              <label class="form-check-label" for="inlineCheckbox2">
                M.Tech
              </label>
            </div>
          </div>
        </div>

        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="inputCity">Gender</label>
            <br />
            <div class="form-check form-check-inline">
              <input
                class="form-check-input"
                type="radio"
                name="inlineRadioOptions"
                id="inlineRadio1"
                value="male"
                checked={userData.gender === "male"}
                onClick={() => {
                  setUserData({
                    ...userData,
                    gender: "male",
                  });
                }}
              />
              <label class="form-check-label" for="inlineRadio1">
                Male
              </label>
            </div>
            <div class="form-check form-check-inline">
              <input
                class="form-check-input"
                type="radio"
                name="inlineRadioOptions"
                id="inlineRadio2"
                value="female"
                checked={userData.gender === "female"}
                onClick={() => {
                  setUserData({
                    ...userData,
                    gender: "female",
                  });
                }}
              />
              <label class="form-check-label" for="inlineRadio2">
                Female
              </label>
            </div>
          </div>
          <div class="form-group col-md-4">
            <label for="inputState">State</label>
            <select
              id="inputState"
              class="form-control"
              value={userData.country}
              onChange={(e) =>
                setUserData({ ...userData, country: e.target.value })
              }
            >
              <option value="">Select the country</option>
              <option value="India">India</option>
              <option value="US">US</option>
              <option value="German">German</option>
            </select>
          </div>
        </div>

        <button type="submit" class="btn btn-primary">
          Submit
        </button>
      </form>
    </div>
  );
}

export default Form;
