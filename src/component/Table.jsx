import React, { useEffect } from "react";
import Nav from "./Nav";

function Table({ data, setData }) {
  useEffect(() => {
    debugger;
    console.log(data, "data");
  }, [JSON.stringify(data)]);

  const editData = (index) => {
    debugger;
    console.log(index);
  };

  const deleteData = (index) => {
    debugger;
    console.log(index);

    let copy = JSON.parse(JSON.stringify(data));
    copy.splice(index, 1);
    debugger;
    setData(copy);
  };

  return (
    <div>
      <Nav />
      <table class="table">
        <tr>
          <th>SL.No</th>
          <th>Name</th>
          <th>Qualification</th>
          <th>Gender</th>
          <th>Country</th>
          <th>Actions</th>
        </tr>
        {data.map((value, index) => {
          return (
            <tr key={index}>
              <td>{parseInt(index) + 1}</td>
              <td>{value.name}</td>
              <td>
                {value.qualBe && value.qualMtech
                  ? `BTech, MTech`
                  : (value.qualBe && "BTech") || (value.qualMtech && "MTech")}
              </td>
              <td>{value.gender}</td>
              <td>{value.country}</td>
              <td>
                <i
                  class="fa fa-pencil"
                  aria-hidden="true"
                  onClick={() => editData(index)}
                ></i>{" "}
                <i
                  class="fa fa-trash"
                  aria-hidden="true"
                  onClick={() => deleteData(index)}
                ></i>
              </td>
            </tr>
          );
        })}
      </table>
      ;
    </div>
  );
}

export default Table;
